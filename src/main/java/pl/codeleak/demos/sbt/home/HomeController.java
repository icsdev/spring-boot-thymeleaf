package pl.codeleak.demos.sbt.home;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
class HomeController {

    @GetMapping("/")
    String index(Model model) {
        model.addAttribute("now", LocalDateTime.now());
        return "index";
    }

    @GetMapping("properties")
    @ResponseBody
    java.util.Properties properties() {
        return System.getProperties();
    }
    
    @GetMapping("/jason")
    String jason(Model model){
    	AssetForm assetForm = new AssetForm();
    	assetForm.setTitle("Runa");
    	assetForm.setDescription("asset request for remote access");
    	model.addAttribute("assetForm", assetForm);
    	model.addAttribute("isTitleRequired",true);
    	return "jason/jason_index";
    }
    
    @PostMapping("/jason")
    String postAssetForm(AssetForm assetForm, RedirectAttributes redirectAttributes){
    	String successMessage = "Title: "+ assetForm.getTitle() + "  Description: "+assetForm.getDescription();
    	System.out.println(successMessage);
    	redirectAttributes.addFlashAttribute("successMessage", successMessage);
    	redirectAttributes.addFlashAttribute("errorMessage", "There was an error processing this request.");
    	return "redirect:/jason";
    }
    
    @GetMapping("/jason_html")
    String jasonHtml(Model model){
    	model.addAttribute("name", "jason");
    	List<String> otherNames = new ArrayList<String>();
    	otherNames.add("Mint");
    	otherNames.add("Gerald");
    	otherNames.add("Archie");
    	otherNames.add("Dan");
    	model.addAttribute("otherNames", otherNames);
    	//return "jason/jason_html :: #content";
    	return "jason/jason_html :: content";
    }
    
    @GetMapping("/jason_json")
    @ResponseBody
    String jasonJson(Model model){
    	return "Jason";
    }
    
    
}
