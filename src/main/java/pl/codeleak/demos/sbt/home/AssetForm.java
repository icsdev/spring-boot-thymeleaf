package pl.codeleak.demos.sbt.home;

public class AssetForm {
	private String title;
	private String description;
	
	public AssetForm(){
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
